MCU = atmega32u4

BOOTLOADER = caterina


BOOTMAGIC_ENABLE = no       # Enable Bootmagic Lite
NKRO_ENABLE = yes           # Enable N-Key Rollover

#~~~
# Compile size optimizations (https://docs.qmk.fm/#/squeezing_avr)

LTO_ENABLE = yes            # Link time optimization
CONSOLE_ENABLE = no         # Console for debug
COMMAND_ENABLE = no         # Commands for debug and configuration
SPACE_CADET_ENABLE = no
GRAVE_ESC_ENABLE = no
MUSIC_ENABLE = no

# RGB stuff
BACKLIGHT_ENABLE = no       # Enable keyboard backlight functionality
RGBLIGHT_ENABLE = no        # Enable keyboard RGB underglow
CUSTOM_MATRIX = lite

AUDIO_ENABLE = no           # Audio output


# project specific files
SRC += matrix.c
QUANTUM_LIB_SRC += uart.c
