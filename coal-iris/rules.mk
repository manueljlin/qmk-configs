
# MCU name
MCU = atmega32u4


# Bootloader selection
BOOTLOADER = atmel-dfu


# Build Options
#   change yes to no to disable
#
BOOTMAGIC_ENABLE = no       # Enable Bootmagic Lite


# Steno
STENO_ENABLE = yes
STENO_PROTOCOL = geminipr
VIRTSER_ENABLE = yes
NKRO_ENABLE = yes           # Enable N-Key Rollover


# Mandatory optimizations to get GeminiPR (steno protocol) to work (its gotta go)
MOUSEKEY_ENABLE = no        # Mouse keys
EXTRAKEY_ENABLE = no        # Audio control and System control


# Compile size optimizations (https://docs.qmk.fm/#/squeezing_avr)
LTO_ENABLE = yes            # Link time optimization
CONSOLE_ENABLE = no         # Console for debug
COMMAND_ENABLE = no         # Commands for debug and configuration
SPACE_CADET_ENABLE = no
GRAVE_ESC_ENABLE = no
MUSIC_ENABLE = no


# Enable combos for the main programming symbols
# COMBO_ENABLE = yes


# RGB stuff
BACKLIGHT_ENABLE = no       # Enable keyboard-wide backlight functionality
RGBLIGHT_ENABLE = no        # Enable keyboard-wide RGB underglow
RGB_MATRIX_ENABLE = yes     # Enable per-key RGB backlight
RGB_MATRIX_DRIVER = WS2812


AUDIO_ENABLE = no           # Audio output
SPLIT_KEYBOARD = yes
ENCODER_ENABLE = no         # I have the model with two thumb switches
