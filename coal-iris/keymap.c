
#include QMK_KEYBOARD_H
#include "keymap_steno.h"


// Use friendly name for the layers
#define _CANARY 0
#define _STENO 1
#define _EXT  2
#define _SYM  3
#define _NUM  4

// Enabling N-Key rollover for Steno
#define FORCE_NKRO

// Saner keycode names for Extend
#define OSM_LALT OSM(MOD_LALT)
#define OSM_LGUI OSM(MOD_LGUI)
#define OSM_LSFT OSM(MOD_LSFT)
#define OSM_LCTL OSM(MOD_LCTL)
#define OSM_RALT OSM(MOD_RALT)

#define ALT_LEFT LALT(KC_LEFT)
#define ALT_RIGHT LALT(KC_RIGHT)

#define ZOOM_LESS LCTL(KC_MINS)
#define ZOOM_MORE LCTL(KC_EQL)

// Set steno serial protocol. Can also use STENO_MODE_BOLT
void matrix_init_user() { steno_set_mode(STENO_MODE_GEMINI); }



enum custom_keycodes {
    // :::: Layers ::::
    CANARY = SAFE_RANGE,
    STENO,
    EXT,
    SYM,
    NUM,

    // :::: Custom keycodes ::::
    C_UPDIR,
};



// :::: Macros ::::
bool process_record_user(uint16_t keycode, keyrecord_t *record) {
    switch (keycode) {

    // Macro for ../ in the Symbols layer
    case C_UPDIR:
        if (record->event.pressed) {
            // When key is pressed
            SEND_STRING("../");
        } else {
            // When key is released
        }
        break;
    }

    return true;
};



const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {



    [_CANARY] = LAYOUT(
    // ╭──────────┬──────────┬──────────┬──────────┬──────────┬──────────╮                              ╭──────────┬──────────┬──────────┬──────────┬──────────┬──────────╮
        KC_ESC    ,KC_1      ,KC_2      ,KC_3      ,KC_4      ,KC_5      ,                               KC_6      ,KC_7      ,KC_8      ,KC_9      ,KC_0      ,KC_DEL    ,
    // ├──────────┼──────────┼──────────┼──────────┼──────────┼──────────┤                              ├──────────┼──────────┼──────────┼──────────┼──────────┼──────────┤
        KC_TAB    ,KC_W      ,KC_L      ,KC_Y      ,KC_P      ,KC_B      ,                               KC_Z      ,KC_F      ,KC_O      ,KC_U      ,KC_QUOT   ,KC_BSLS   ,
    // ├──────────┼──────────┼──────────┼──────────┼──────────┼──────────┤                              ├──────────┼──────────┼──────────┼──────────┼──────────┼──────────┤
        KC_BSPC   ,KC_C      ,KC_R      ,KC_S      ,KC_T      ,KC_G      ,                               KC_M      ,KC_N      ,KC_E      ,KC_I      ,KC_A      ,KC_ENT    ,
    // ├──────────┼──────────┼──────────┼──────────┼──────────┼──────────┼──────────╮        ╭──────────┼──────────┼──────────┼──────────┼──────────┼──────────┼──────────┤
        KC_LGUI   ,KC_Q      ,KC_J      ,KC_V      ,KC_D      ,KC_K      ,KC_SPC    ,         TG(_STENO),KC_X      ,KC_H      ,KC_SLSH   ,KC_COMM   ,KC_DOT    ,KC_NO     ,
    // ╰──────────┴──────────┴──────────┴──────────┼──────────┼──────────┼──────────┤        ├──────────┼──────────┼──────────┼──────────┴──────────┴──────────┴──────────╯
                                                    OSM_RALT  ,MO(_EXT)  ,KC_SPC    ,         KC_LSFT   ,MO(_SYM)  ,OSM_RALT
                                                // ╰──────────┴──────────┴──────────╯        ╰──────────┴──────────┴──────────╯
    ),



    /*  :::: STENO LAYER ::::

        ╭───┬───┬───┬───┬───┬───╮   ╭───┬───┬───┬───┬───┬───╮
        | 1 | 2 | 3 | 4 | 5 | 6 |   | 7 | 8 | 9 | A | B | C | // Number shortcuts
        ╰───┴───┴───┴───┴───┴───╯   ╰───┴───┴───┴───┴───┴───╯

        ╭───┬───┬───┬───┬───┬───╮   ╭───┬───┬───┬───┬───┬───╮
        |FN |   | T | P | H |   |   |   | F | P | L | T | D |
        ├───┤ S ├───┼───┼───| * |   | * ├───┼───┼───┼───┼───┤
        |   |   | K | W | R |   |   |   | R | B | G | S | Z |
        ╰───┴───┴───┴───┼───┼───|   |───|───|───┴───┴───┴───╯
                        | A | O |   | E | U |
                        ╰───┴───╯   ╰───┴───╯
    */

    [_STENO] = LAYOUT(
    // ╭──────────┬──────────┬──────────┬──────────┬──────────┬──────────╮                              ╭──────────┬──────────┬──────────┬──────────┬──────────┬──────────╮
        STN_N1    ,STN_N2    ,STN_N3    ,STN_N4    ,STN_N5    ,STN_N6    ,                               STN_N7    ,STN_N8    ,STN_N9    ,STN_NA    ,STN_NB    ,STN_NC    ,
    // ├──────────┼──────────┼──────────┼──────────┼──────────┼──────────┤                              ├──────────┼──────────┼──────────┼──────────┼──────────┼──────────┤
        STN_FN    ,STN_S1    ,STN_TL    ,STN_PL    ,STN_HL    ,STN_ST1   ,                               STN_ST3   ,STN_FR    ,STN_PR    ,STN_LR    ,STN_TR    ,STN_DR    ,
    // ├──────────┼──────────┼──────────┼──────────┼──────────┼──────────┤                              ├──────────┼──────────┼──────────┼──────────┼──────────┼──────────┤
        KC_NO     ,STN_S2    ,STN_KL    ,STN_WL    ,STN_RL    ,STN_ST2   ,                               STN_ST4   ,STN_RR    ,STN_BR    ,STN_GR    ,STN_SR    ,STN_ZR    ,
    // ├──────────┼──────────┼──────────┼──────────┼──────────┼──────────┼──────────╮        ╭──────────┼──────────┼──────────┼──────────┼──────────┼──────────┼──────────┤
        KC_TRNS   ,KC_NO     ,KC_NO     ,KC_NO     ,KC_NO     ,KC_NO     ,KC_TRNS   ,         KC_TRNS   ,KC_NO     ,KC_NO     ,KC_NO     ,KC_NO     ,KC_NO     ,KC_NO     ,
    // ╰──────────┴──────────┴──────────┴──────────┼──────────┼──────────┼──────────┤        ├──────────┼──────────┼──────────┼──────────┴──────────┴──────────┴──────────╯
                                                    STN_A     ,STN_O     ,KC_TRNS   ,         KC_LSFT   ,STN_E     ,STN_U
                                                // ╰──────────┴──────────┴──────────╯        ╰──────────┴──────────┴──────────╯
    ),



    /*  :::: EXTEND LAYER ::::
        ╭───┬───┬───┬───┬───╮   ╭───┬───┬───┬───┬───╮
        |esc|prv|*f |nxt|ins|   |pup|*- | < |*+ |cap|
        ├───┼───┼───┼───┼───┤   ├───┼───┼───┼───┼───┤
        |alt|sup|shf|ctr|agr|   |pdw| < | v | > |del|
        ├───┼───┼───┼───┼───┤   ├───┼───┼───┼───┼───┤
        |*z |*x |*c |tab|*v |   |ent|bkp|   |mnu|prt|
        ╰───┴───┴───┴───┴───╯   ╰───┴───┴───┴───┴───╯
    */

    [_EXT] = LAYOUT(
    // ╭──────────┬──────────┬──────────┬──────────┬──────────┬──────────╮                              ╭──────────┬──────────┬──────────┬──────────┬──────────┬──────────╮
        KC_TRNS   ,KC_NO     ,KC_NO     ,KC_NO     ,KC_NO     ,KC_NO     ,                               KC_NO     ,KC_NO     ,KC_NO     ,KC_NO     ,KC_NO     ,KC_TRNS   ,
    // ├──────────┼──────────┼──────────┼──────────┼──────────┼──────────┤                              ├──────────┼──────────┼──────────┼──────────┼──────────┼──────────┤
        KC_TRNS   ,KC_ESC    ,ALT_LEFT  ,LCTL(KC_F),ALT_RIGHT ,KC_INS    ,                               KC_PGUP   ,ZOOM_LESS ,KC_UP     ,ZOOM_MORE ,KC_CAPS   ,KC_TRNS   ,
    // ├──────────┼──────────┼──────────┼──────────┼──────────┼──────────┤                              ├──────────┼──────────┼──────────┼──────────┼──────────┼──────────┤
        KC_TRNS   ,OSM_LALT  ,OSM_LGUI  ,OSM_LSFT  ,OSM_LCTL  ,OSM_RALT  ,                               KC_PGDN   ,KC_LEFT   ,KC_DOWN   ,KC_RGHT   ,KC_DEL    ,KC_TRNS   ,
    // ├──────────┼──────────┼──────────┼──────────┼──────────┼──────────┼──────────╮        ╭──────────┼──────────┼──────────┼──────────┼──────────┼──────────┼──────────┤
        KC_TRNS   ,LCTL(KC_Z),LCTL(KC_X),LCTL(KC_C),KC_TAB    ,LCTL(KC_V),KC_TRNS   ,         KC_TRNS   ,KC_ENT    ,KC_BSPC   ,KC_NO     ,KC_APP    ,KC_PSCR   ,KC_TRNS   ,
    // ╰──────────┴──────────┴──────────┴──────────┼──────────┼──────────┼──────────┤        ├──────────┼──────────┼──────────┼──────────┴──────────┴──────────┴──────────╯
                                                    KC_TRNS   ,KC_TRNS   ,KC_TRNS   ,         KC_ENT    ,MO(_NUM)  ,KC_TRNS
                                                // ╰──────────┴──────────┴──────────╯        ╰──────────┴──────────┴──────────╯
    ),



    /*  :::: SYMBOLS LAYER ::::
        ╭───┬───┬───┬───┬───╮   ╭───┬───┬───┬───┬───╮
        | $ | { | } | * | ^ |   | % | @ | < | > |../|
        ├───┼───┼───┼───┼───┤   ├───┼───┼───┼───┼───┤
        | / | ( | ) | _ | & |   | # | = | _ | : | ! |
        ├───┼───┼───┼───┼───┤   ├───┼───┼───┼───┼───┤
        | ~ | [ | ] | ; | | |   | ` | " | + | \ | ? |
        ╰───┴───┴───┴───┴───╯   ╰───┴───┴───┴───┴───╯
    */

    [_SYM] = LAYOUT(
    // ╭──────────┬──────────┬──────────┬──────────┬──────────┬──────────╮                              ╭──────────┬──────────┬──────────┬──────────┬──────────┬──────────╮
        KC_TRNS   ,KC_NO     ,KC_NO     ,KC_NO     ,KC_NO     ,KC_NO     ,                               KC_NO     ,KC_NO     ,KC_NO     ,KC_NO     ,KC_NO     ,KC_TRNS   ,
    // ├──────────┼──────────┼──────────┼──────────┼──────────┼──────────┤                              ├──────────┼──────────┼──────────┼──────────┼──────────┼──────────┤
        KC_TRNS   ,KC_DLR    ,KC_LCBR   ,KC_RCBR   ,KC_ASTR   ,KC_CIRC   ,                               KC_PERC   ,KC_AT     ,KC_LT     ,KC_GT     ,C_UPDIR,   KC_TRNS   ,
    // ├──────────┼──────────┼──────────┼──────────┼──────────┼──────────┤                              ├──────────┼──────────┼──────────┼──────────┼──────────┼──────────┤
        KC_TRNS   ,KC_SLSH   ,KC_LPRN   ,KC_RPRN   ,KC_UNDS   ,KC_AMPR   ,                               KC_HASH   ,KC_EQL    ,KC_MINS   ,KC_COLN   ,KC_EXLM   ,KC_TRNS   ,
    // ├──────────┼──────────┼──────────┼──────────┼──────────┼──────────┼──────────╮        ╭──────────┼──────────┼──────────┼──────────┼──────────┼──────────┼──────────┤
        KC_TRNS   ,KC_TILD   ,KC_LBRC   ,KC_RBRC   ,KC_SCLN   ,KC_PIPE   ,KC_TRNS   ,         KC_TRNS   ,KC_GRV    ,KC_DQUO   ,KC_PLUS   ,KC_BSLS   ,KC_QUES   ,KC_TRNS   ,
    // ╰──────────┴──────────┴──────────┴──────────┼──────────┼──────────┼──────────┤        ├──────────┼──────────┼──────────┼──────────┴──────────┴──────────┴──────────╯
                                                    KC_TRNS   ,MO(_NUM)  ,KC_TRNS   ,         KC_TRNS   ,KC_TRNS   ,KC_TRNS
                                                // ╰──────────┴──────────┴──────────╯        ╰──────────┴──────────┴──────────╯
    ),



    /*  :::: NUM LAYER ::::
        ╭───┬───┬───┬───┬───╮   ╭───┬───┬───┬───┬───╮
        | , | 1 | 2 | 3 |   |   |   |F1 |F2 |F3 |F10|
        ├───┼───┼───┼───┼───┤   ├───┼───┼───┼───┼───┤
        | 0 | 4 | 5 | 6 |   |   |   |F4 |F5 |F6 |F11|
        ├───┼───┼───┼───┼───┤   ├───┼───┼───┼───┼───┤
        | . | 7 | 8 | 9 |   |   |   |F7 |F8 |F9 |F12|
        ╰───┴───┴───┴───┴───╯   ╰───┴───┴───┴───┴───╯
    */

    [_NUM] = LAYOUT(
    // ╭──────────┬──────────┬──────────┬──────────┬──────────┬──────────╮                              ╭──────────┬──────────┬──────────┬──────────┬──────────┬──────────╮
        RGB_TOG   ,RGB_VAD   ,RGB_VAI   ,RGB_SAD   ,RGB_SAI   ,RGB_RMOD  ,                               RGB_MOD   ,RGB_HUD   ,RGB_HUI   ,RGB_SPD   ,RGB_SPI   ,KC_TRNS   ,
    // ├──────────┼──────────┼──────────┼──────────┼──────────┼──────────┤                              ├──────────┼──────────┼──────────┼──────────┼──────────┼──────────┤
        KC_TRNS   ,KC_COMM   ,KC_1      ,KC_2      ,KC_3      ,KC_NO     ,                               KC_NO     ,KC_F1     ,KC_F2     ,KC_F3     ,KC_F10    ,KC_TRNS   ,
    // ├──────────┼──────────┼──────────┼──────────┼──────────┼──────────┤                              ├──────────┼──────────┼──────────┼──────────┼──────────┼──────────┤
        KC_TRNS   ,KC_0      ,KC_4      ,KC_5      ,KC_6      ,KC_NO     ,                               KC_NO     ,KC_F4     ,KC_F5     ,KC_F6     ,KC_F11    ,KC_TRNS   ,
    // ├──────────┼──────────┼──────────┼──────────┼──────────┼──────────┼──────────╮        ╭──────────┼──────────┼──────────┼──────────┼──────────┼──────────┼──────────┤
        KC_TRNS   ,KC_DOT    ,KC_7      ,KC_8      ,KC_9      ,KC_NO     ,KC_TRNS   ,         KC_TRNS   ,KC_NO     ,KC_F7     ,KC_F8     ,KC_F9     ,KC_F12    ,KC_TRNS   ,
    // ╰──────────┴──────────┴──────────┴──────────┼──────────┼──────────┼──────────┤        ├──────────┼──────────┼──────────┼──────────┴──────────┴──────────┴──────────╯
                                                    KC_TRNS   ,KC_TRNS   ,KC_TRNS   ,         KC_TRNS   ,KC_TRNS   ,KC_TRNS
                                                // ╰──────────┴──────────┴──────────╯        ╰──────────┴──────────┴──────────╯
    ),
};

// enum combos {
//     // Left side
//     L_Y_LT,   // L + Y = <
//     C_R_LBRC, // C + R = [
//     R_S_LCBR, // R + S = {
//     S_T_LPRN, // S + T = (
//
//     // Right side
//     O_U_GT,   // O + U = >
//     N_E_RCBR, // N + E = )
//     E_I_RCBR,   // E + I = }
//     I_A_, // I + A = ]
// };
//
// const uint16_t PROGMEM s_t_lprn[] = { KC_S, KC_T, COMBO_END};
// const uint16_t PROGMEM r_s_lt[]   = { KC_R, KC_S, COMBO_END};
// const uint16_t PROGMEM c_r_lbrc[] = { KC_C, KC_R, COMBO_END};
// const uint16_t PROGMEM l_y_lcbr[] = { KC_L, KC_Y, COMBO_END};
// const uint16_t PROGMEM n_e_rprn[] = { KC_N, KC_E, COMBO_END};
// const uint16_t PROGMEM e_i_gt[]   = { KC_E, KC_I, COMBO_END};
// const uint16_t PROGMEM i_a_rcbr[] = { KC_I, KC_A, COMBO_END};
// const uint16_t PROGMEM o_u_rbrc[] = { KC_O, KC_U, COMBO_END};
//
// combo_t key_combos[COMBO_COUNT] = {
//     [S_T_LPRN] = COMBO(s_t_lprn, KC_LPRN),
//     [R_S_LT]   = COMBO(r_s_lt, KC_LT),
//     [C_R_LBRC] = COMBO(c_r_lbrc, KC_LBRC),
//     [L_Y_LCBR] = COMBO(l_y_lcbr, KC_LCBR),
//     [N_E_RPRN] = COMBO(n_e_rprn, KC_RPRN),
//     [E_I_GT]   = COMBO(e_i_gt, KC_GT),
//     [I_A_RCBR] = COMBO(i_a_rcbr, KC_RCBR),
//     [O_U_RBRC] = COMBO(o_u_rbrc, KC_RBRC),
// };
