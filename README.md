# qmk-configs
Coal. Because what if Seniply, but worse.

![img](./coal.png)

## How to do the thing

1. Install QMK and run `qmk setup`

2. If running NixOS, move to `~/qmk_firmware` and execute `nix-shell ./shell.nix` to open a new shell with all of QMK set up. It will take a while as it will compile most of the dependencies.

3. Move the layout to the keymap folder of the specific keyboard.

4. Flash it using `qmk flash`. You can also try `qmk compile` after doing changes to see if it'll still compile.
  - `qmk flash -kb redox_w -km coal-redoxw`
  - `qmk flash -kb keebio/iris/rev6 coal-iris`

5. Press the reset button on the controller in order for QMK to detect it and flash the firmware.
